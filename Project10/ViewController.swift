//
//  ViewController.swift
//  Project10
//
//  Created by Muri Gumbodete on 25/03/2022.
//

import UIKit

class ViewController: UICollectionViewController {
  
  // MARK: - Variables
  var people = [Person]()
  
  //MARK: - UIViewControllerEvents
  override func viewDidLoad() {
    super.viewDidLoad()
    
    navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addNewPerson))
    
    let defaults = UserDefaults.standard
    
    if let savedPeople = defaults.object(forKey: "people") as? Data {
      if let decodePeople = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(savedPeople) as? [Person] {
        people = decodePeople
      }
    }
  }
  
  @objc func addNewPerson() {
    let picker = UIImagePickerController()
    picker.allowsEditing = true
    picker.delegate = self
    if UIImagePickerController.isSourceTypeAvailable(.camera) == true {
      let ac = UIAlertController(title: "Source of image", message: "How would you like to show image?", preferredStyle: .alert)
      ac.addAction(UIAlertAction(title: "Camera", style: .default) { action in
        picker.sourceType = .camera
        self.present(picker, animated: true)
      })
      ac.addAction(UIAlertAction(title: "Photo Library", style: .default) { action in
        picker.sourceType = .photoLibrary
        self.present(picker, animated: true)
      })
      present(ac, animated: true)
    } else { present(picker, animated: true) }
    
  }
  
  func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
  }
  
  func save() {
    if let savedData = try? NSKeyedArchiver.archivedData(withRootObject: people, requiringSecureCoding: false) {
      let defaults = UserDefaults.standard
      defaults.set(savedData, forKey: "people")
    }
  }
}

// MARK: - CollectionViewDataSource
extension ViewController {
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return people.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Person", for: indexPath) as? PersonCell else { fatalError("Unable to dequeue PersonCell") }
    
    let person = people[indexPath.item]
    
    cell.name.text = person.name
    
    let path = getDocumentsDirectory().appendingPathComponent(person.image)
    cell.imageView.image = UIImage(contentsOfFile: path.path)
    
    cell.imageView.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
    cell.imageView.layer.borderWidth = 2
    cell.imageView.layer.cornerRadius = 3
    cell.layer.cornerRadius = 7
    
    return cell
  }
}

// MARK: - CollectionViewDelegate
extension ViewController {
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let person = people[indexPath.item]
    
    let alert = UIAlertController(title: "Rename or Delete", message: "Would you like to rename user or delete?", preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Delete", style: .cancel))
    alert.addAction(UIAlertAction(title: "Rename", style: .default) { action in
      let ac = UIAlertController(title: "Remane Person", message: nil, preferredStyle: .alert)
      ac.addTextField()
      
      ac.addAction(UIAlertAction(title: "OK", style: .default) { [weak self, weak ac] _ in
        guard let newName = ac?.textFields?[0].text else { return }
        person.name = newName
        self?.save()
        self?.collectionView.reloadData()
      })
      ac.addAction(UIAlertAction(title: "Cancel", style: .cancel))
      
      self.present(ac, animated: true)
    })
    present(alert, animated: true)
  }
}

// MARK: - UIImagePickerDelegate
extension ViewController: UIImagePickerControllerDelegate & UINavigationControllerDelegate{
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let image = info[.editedImage] as? UIImage else { return }
    
    let imageName = UUID().uuidString
    let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
    
    if let jpegData = image.jpegData(compressionQuality: 0.8) {
      try? jpegData.write(to: imagePath)
    }
    
    let person = Person(name: "Unknown", image: imageName)
    people.append(person)
    save()
    collectionView.reloadData()
    dismiss(animated: true)
  }
}

